<?php

/**
 * FONT AWESOME
 * 
 * Description:
 * 
 * @link https://fontawesome.com/   
 */ 

    function awsm_child_load_fontawesome() {
        /**
         * You can find the current URL for the latest version here: https://fontawesome.com/start 
         */
        wp_enqueue_style( 'fontawesome-free', '//use.fontawesome.com/releases/v5.12.0/css/all.css' );
    }

add_action( 'wp_enqueue_scripts', 'awsm_child_load_fontawesome' );
