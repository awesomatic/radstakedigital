<?php

/** Credits */

function awsm_credits() {
	echo 'Powered by <a href="https://awesomatic.nl/">Awesomatic</a>';
	echo ' & <a href="https://radstakedigital.nl/">Radstake Digital</a>';
}
