<?php

/**
 * SIDEBARS
 *
 * A sidebar is any widgetized area of your theme. 
 * Widget areas are places in your theme where users can add their own widgets. 
 * 
 * @link https://developer.wordpress.org/themes/functionality/sidebars/
 */

function awsm_child_register_sidebar() {

	/**
	 * HEADER	
	 */ 

	register_sidebar( array(

		// your name for the sidebar. This is the name users will see in the Widgets panel.
		'name'          => 'Header',

		// must be lowercase. You will call this in your theme using the dynamic_sidebar function.
		'id'            => 'sidebar_header', 

		// A description of the sidebar. This will also be shown in the admin Widgets panel.

		'description'	=> '',

		//  The CSS class name to assign to the widget’s HTML.
		'class' 		=> '', 		

		// HTML that is placed before every widget.
		'before_widget' => '<div class="widget">',

		// HTML that is placed after every widget. Should be used to close tags from before_widget.
		'after_widget'  => '</div>',

		// HTML that is placed before the title of each widget, such as a header tag.
		'before_title'  => '<h2>',

		// HTML that is placed after every title. Should be used to close tags from before_title.
		'after_title'   => '</h2>',

	) );

	/**
	 * Footer
	 */ 

	register_sidebar( array(

		// your name for the sidebar. This is the name users will see in the Widgets panel.
		'name'          => 'Footer',

		// must be lowercase. You will call this in your theme using the dynamic_sidebar function.
		'id'            => 'sidebar_footer', 

		// A description of the sidebar. This will also be shown in the admin Widgets panel.

		'description'	=> '',

		//  The CSS class name to assign to the widget’s HTML.
		'class' 		=> '', 		

		// HTML that is placed before every widget.
		'before_widget' => '<div class="widget">',

		// HTML that is placed after every widget. Should be used to close tags from before_widget.
		'after_widget'  => '</div>',

		// HTML that is placed before the title of each widget, such as a header tag.
		'before_title'  => '<h2>',

		// HTML that is placed after every title. Should be used to close tags from before_title.
		'after_title'   => '</h2>',

	) );

	/**
	 * Shop
	 */ 

	register_sidebar( array(

		// your name for the sidebar. This is the name users will see in the Widgets panel.
		'name'          => 'Shop',

		// must be lowercase. You will call this in your theme using the dynamic_sidebar function.
		'id'            => 'shop', 

		// A description of the sidebar. This will also be shown in the admin Widgets panel.

		'description'	=> '',

		//  The CSS class name to assign to the widget’s HTML.
		'class' 		=> '', 		

		// HTML that is placed before every widget.
		'before_widget' => '<div class="widget">',

		// HTML that is placed after every widget. Should be used to close tags from before_widget.
		'after_widget'  => '</div>',

		// HTML that is placed before the title of each widget, such as a header tag.
		'before_title'  => '<h2>',

		// HTML that is placed after every title. Should be used to close tags from before_title.
		'after_title'   => '</h2>',

	) );

}

add_action( 'widgets_init', 'awsm_child_register_sidebar' );
