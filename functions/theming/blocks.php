<?php 

/**
 * GUTENBERG COLOR PALETTE
 */

    function awsm_child_add_color_palette() {

        $color = get_field('acf_fields_colors', 'option'); 
        $color_primary = esc_attr($color['acf_field_color_primary']);
        $color_secondary = esc_attr($color['acf_field_color_secondary']);
        $color_dark = esc_attr($color['acf_field_color_dark']);
        $color_light = esc_attr($color['acf_field_color_light']);
        
        /**
         * We don't want users screwing up the theme, so:
         */
        add_theme_support('disable-custom-colors');
        /**
         * Instead, add support for a custom color scheme. 
         * Users can fill in their brand colors via a color picker.
         */
        add_theme_support('editor-color-palette', array(
            /*
            * Primary brand color
            */
            array(
                'name'  => __('Primary', 'awsm-starter'),
                'slug'  => 'primary',
                'color' => $color_primary, // This is the color that show up in the Gutenberg editor
            ),
            /*
            * Secondary brand color
            */
            array(
                'name'  => __('Secondary', 'awsm-starter'),
                'slug'  => 'secondary',

                // This is the color that show up in the Gutenberg editor
                'color' => $color_secondary,
            ),
            /*
            * Primary brand color
            */
            array(
                'name'  => __('Dark', 'awsm-starter'),
                'slug'  => 'Dark',

                // This is the color that show up in the Gutenberg editor
                'color' => $color_dark,
            ),
            /*
            * Secondary brand color
            */
            array(
                'name'  => __('Light', 'awsm-starter'),
                'slug'  => 'Light',

                // This is the color that show up in the Gutenberg editor
                'color' => $color_light,
            ),
        ));
    }

add_action('after_setup_theme', 'awsm_child_add_color_palette');

// Hero block: the block

add_action('acf/init', 'my_acf_init');

function my_acf_init() {
	
	// check of de functie bestaat
	if( function_exists('acf_register_block') ) {
		
		// Custom Testimonail block registrerend
		acf_register_block(array(
			'name'				=> 'Hero',
			'title'				=> __('Hero'),
			'description'		=> __('Een block voor op de home, te gebruiken als header.'),
            //'render_callback'	=> 'my_acf_block_render_callback',
            'render_template'   => 'parts/01-molecules/02-blocks/awsm-hero.php',
			'category'			=> 'layout', // Zie https://wordpress.org/gutenberg/handbook/block-api/
			'icon'				=> 'admin-links', // Dashicon icon class naam
			'keywords'			=> array( 'hero', 'freelancer' ),
		));
				
	}
}

// Hero block: the fields

if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5efb487177a0e',
        'title' => 'Awesomatic Hero',
        'fields' => array(
            array(
                'key' => 'field_5efb491b4eea7',
                'label' => 'Subkop',
                'name' => 'awsm_field_hero__subtitle',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5efb48c44eea6',
                'label' => 'Kop',
                'name' => 'awsm_field_hero__title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5efb48854eea5',
                'label' => 'Afbeelding',
                'name' => 'awsm_field_hero__img',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'medium',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array(
                'key' => 'field_5efb4972a667c',
                'label' => 'Knop',
                'name' => 'awsm_field__hero__button',
                'type' => 'button_group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                ),
                'allow_null' => 0,
                'default_value' => '',
                'layout' => 'horizontal',
                'return_format' => 'value',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'block',
                    'operator' => '==',
                    'value' => 'acf/hero',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
    endif;

    // Hero block: Render

    // function my_acf_block_render_callback( $block ) {
	
    //     // convert name ("acf/testimonial") into path friendly slug ("testimonial")
    //     $slug = str_replace('acf/', '', $block['name']);
        
    //     // template bestand vanuit de "template-parts/block" map includen
    //     if( file_exists(STYLESHEETPATH . "/blocks/content-{$slug}.php") ) {
    //         include( STYLESHEETPATH . "/blocks/content-{$slug}.php" );
    //     }
    // }
