/**
 * TOGGLE HAMBURGER
 * 
 * Description: 
 * 
 * @doc awesomatic.nl/support/docs/
 ********************************/

jQuery(function($) {
  /**
   * If hamburger is clicked ..
   */
    $(".hamburger").click(function() {
      /**
       * .. add a class "visible" to the .hamburger ..
       */
      $(".hamburger").toggleClass("fixed");
      /**
       * .. and add a class "visible" so you can style it with css
       */
      $(".header-modal-window").toggleClass("visible");
      /**
       * .. and add a class "freeze" to prevent the body from scolling
       */
      $("body").toggleClass("freeze");
    });
});
  
  // Look for .hamburger
  var hamburger = document.querySelector(".hamburger");
  // On click
  hamburger.addEventListener("click", function() {
    // Toggle class "is-active"
    hamburger.classList.toggle("is-active");
    // Do something else, like open/close menu
  });
