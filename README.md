# Sample Awesomatic Child Theme

A sample child theme that can be used with ANY theme.

## Instructions

* First edit the functions.php file and change it to match the parent theme.
* Next edit style.css and change the Template value to match your parent theme name.
* Now you can upload it to your server and activate it and use it - Yay!
