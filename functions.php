<?php
/**
 * CHILD THEME FUNCTIONS
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * @link http://codex.wordpress.org/Child_Themes
 */

/**
 * TABLE OF CONTENTS
 *
 * CORE............................Everything thats needed to run a WordPress Child Theme properly
 * Scripts.........................Enqueue parent and child styles
 *
 * EXTENSIONS......................Extensions and modifications to WordPress plugins, js libraries, css libraries, etc.
 * Google Fonts....................Enqueue or load custom fonts
 * FontAwesome.....................Enqueue Font Awesome Version 5.10.2
 * WooCommerce.....................
 *
 * THEMING.........................Give that front-end developer some stuff to work with :)
 * Menus...........................Register our menus
 * Sidebars........................Register our sidebars and widgetized areas
 * Template Tags...................Display information dynamically or otherwise customize your site
 * Blocks..........................
 */

/* CORE
*******/

require get_stylesheet_directory() . '/functions/core/scripts.php';

/* EXTENSIONS
*************/

require get_stylesheet_directory() . '/functions/extensions/fontawesome.php';
require get_stylesheet_directory() . '/functions/extensions/woocommerce.php';

/* THEMING
**********/

require get_stylesheet_directory() . '/functions/theming/menus.php';
require get_stylesheet_directory() . '/functions/theming/sidebars.php';
require get_stylesheet_directory() . '/functions/theming/template-tags.php';
require get_stylesheet_directory() . '/functions/theming/blocks.php';

//Remove Gutenberg Block Library CSS from loading on the frontend
// function smartwp_remove_wp_block_library_css(){
//     wp_dequeue_style( 'wp-block-library' );
//     wp_dequeue_style( 'wp-block-library-theme' );
//     wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
//    }
//    add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );
