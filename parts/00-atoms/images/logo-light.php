<?php
	the_custom_logo();
		if ( is_front_page() && is_home() ) : ?>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php else : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		<?php
		endif;

		$description = get_bloginfo( 'description', 'display' );
						
		if ( $description || is_customize_preview() ) : ?>

<?php if (get_field('awsm_site_description', 'option')) : ?>

			<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>

			<?php endif ?>
		<?php endif; ?>
                    